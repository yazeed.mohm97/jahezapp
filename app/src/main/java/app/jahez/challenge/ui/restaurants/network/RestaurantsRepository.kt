package app.jahez.challenge.ui.restaurants.network

import android.util.Log
import androidx.lifecycle.MutableLiveData
import app.jahez.challenge.ui.restaurants.models.Restaurant
import app.jahez.challenge.utilities.network.EndPoints
import app.jahez.challenge.utilities.network.NetworkClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

class RestaurantsRepository {
    companion object {
        private var instance: RestaurantsRepository? = null

        fun getInstance(): RestaurantsRepository? {
            if (instance == null) instance = RestaurantsRepository()
            return instance
        }
    }

    fun getRestaurants(): MutableLiveData<ArrayList<Restaurant>?> {
        Log.i("getRestaurants", "im in getRestaurants")
        val restaurants: MutableLiveData<ArrayList<Restaurant>?> = MutableLiveData<ArrayList<Restaurant>?>()
        val retrofit: Retrofit? = NetworkClient.getRetrofitClient()
        val api: EndPoints = retrofit!!.create(EndPoints::class.java)
        val getData: Call<ArrayList<Restaurant>> = api.getRestaurants()
        getData.enqueue(object : Callback<ArrayList<Restaurant>?> {
            override fun onResponse(call: Call<ArrayList<Restaurant>?>, response: Response<ArrayList<Restaurant>?>) {
                if (response.isSuccessful) {
                    Log.i("getRestaurants", response.body().toString())
                    restaurants.value = response.body()
                } else {
                    Log.i("getRestaurants", response.message().toString())
                    restaurants.value = null
                }
            }

            override fun onFailure(call: Call<ArrayList<Restaurant>?>, t: Throwable) {
                Log.i("getRestaurants", t.message!!)
                restaurants.value = null
            }

        })
        return restaurants
    }

}