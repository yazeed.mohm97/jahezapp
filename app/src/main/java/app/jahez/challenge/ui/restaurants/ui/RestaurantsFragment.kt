package app.jahez.challenge.ui.restaurants.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.ComponentActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import app.jahez.challenge.databinding.FragmentRestaurantsBinding
import app.jahez.challenge.ui.restaurants.viewModels.RestaurantsViewModel

class RestaurantsFragment : Fragment() {

    private var _binding: FragmentRestaurantsBinding? = null

    private val rViewModel by lazy {
        ViewModelProvider(this)[RestaurantsViewModel::class.java]
    }

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentRestaurantsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        rViewModel.initRestaurants()
        rViewModel.getRestaurants()?.observe(viewLifecycleOwner, { restaurants ->
            if (!restaurants.isNullOrEmpty()) {
                binding.progressBar.visibility = View.GONE
                binding.restaurantsList.adapter = RestaurantsAdapter(requireActivity(), restaurants.sortedBy { it.distance })
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}