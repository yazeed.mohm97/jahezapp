package app.jahez.challenge.ui.settings

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import app.jahez.challenge.MainActivity
import app.jahez.challenge.R
import app.jahez.challenge.databinding.FragmentSettingsBinding
import app.jahez.challenge.ui.splashScreen.SplashScreen
import app.jahez.challenge.utilities.VariablesUtils
import app.jahez.challenge.utilities.mUtils

class SettingsFragment : Fragment() {

    private lateinit var settingsViewModel: SettingsViewModel
    private var _binding: FragmentSettingsBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentSettingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    private fun init() {
        binding.language.text = getString(R.string.lang)

        binding.languageCard.setOnClickListener {
            val lang = if (VariablesUtils.language == "en") "ar" else "en"
            mUtils.setLocaleLang(lang, requireActivity()).apply {
                startActivity(Intent(requireActivity(), SplashScreen::class.java))
                requireActivity().finish()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}