package app.jahez.challenge.ui.restaurants.ui

import android.content.Context
import android.os.Build
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import app.jahez.challenge.R
import app.jahez.challenge.ui.restaurants.models.Restaurant
import app.jahez.challenge.utilities.mUtils
import com.bumptech.glide.Glide
import okio.ByteString.Companion.decodeBase64
import java.net.URLDecoder
import java.net.URLEncoder
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

class RestaurantsAdapter(private val context: Context, private val items: List<Restaurant>) : RecyclerView.Adapter<RestaurantsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.restaurant_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        holder.itemName.text = item.name
        holder.itemDistance.text = "${mUtils.round(item.distance, 2)} Kms"
        holder.itemDesc.text = if (item.description!!.toCharArray().size > 35) "${item.description!!.substring(0, 30)} ..." else item.description

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            holder.itemStatus.text = if (mUtils.checkStatus(item.hours!!)) context.getString(R.string.open) else context.getString(R.string.close)
            holder.itemStatus.setTextColor(if (mUtils.checkStatus(item.hours!!)) context.getColor(R.color.green) else context.getColor(R.color.red))
        }

        holder.itemHours.text = item.hours

        if (item.hasOffer) {
            holder.itemOffer.visibility = View.VISIBLE
            holder.itemOffer.text = item.offer
        }

        Glide.with(context).load(item.image).into(holder.itemImage)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val item: CardView = itemView.findViewById(R.id.item)
        val itemImage: ImageView = itemView.findViewById(R.id.itemImage)
        val itemName: TextView = itemView.findViewById(R.id.itemName)
        val itemDistance: TextView = itemView.findViewById(R.id.itemDistance)
        val itemDesc: TextView = itemView.findViewById(R.id.itemDesc)
        val itemStatus: TextView = itemView.findViewById(R.id.itemStatus)
        val itemHours: TextView = itemView.findViewById(R.id.itemHours)
        val itemOffer: TextView = itemView.findViewById(R.id.itemOffer)
    }

}

