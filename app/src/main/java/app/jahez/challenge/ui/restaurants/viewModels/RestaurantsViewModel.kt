package app.jahez.challenge.ui.restaurants.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.jahez.challenge.ui.restaurants.models.Restaurant
import app.jahez.challenge.ui.restaurants.network.RestaurantsRepository

class RestaurantsViewModel : ViewModel() {
    private var restaurants: MutableLiveData<ArrayList<Restaurant>?>? = null

    fun initRestaurants() {
        if (restaurants != null) {
            return
        }
        val repository = RestaurantsRepository.getInstance()
        restaurants = repository!!.getRestaurants()
    }

    fun getRestaurants(): LiveData<ArrayList<Restaurant>?>? {
        return restaurants
    }


}