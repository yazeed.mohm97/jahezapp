package app.jahez.challenge.ui.restaurants.models

import androidx.room.Entity
import androidx.room.PrimaryKey


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

@Entity(tableName = "restaurants")
class Restaurant(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var name: String? = null,
    var description: String? = null,
    var hours: String? = null,
    var image: String? = null,
    var rating: Double = 0.0,
    var distance: Double = 0.0,
    var hasOffer: Boolean = false,
    var offer: String? = null
)
