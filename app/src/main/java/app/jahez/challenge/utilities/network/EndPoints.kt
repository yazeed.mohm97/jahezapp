package app.jahez.challenge.utilities.network

import app.jahez.challenge.ui.restaurants.models.Restaurant
import retrofit2.Call
import retrofit2.http.GET

interface EndPoints {
    @GET("restaurants.json")
    fun getRestaurants(): Call<ArrayList<Restaurant>>
}