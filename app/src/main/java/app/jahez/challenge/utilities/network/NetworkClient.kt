package app.jahez.challenge.utilities.network

import app.jahez.challenge.utilities.VariablesUtils
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

class NetworkClient {

    companion object {
        private var retrofit: Retrofit? = null

        fun getRetrofitClient(): Retrofit? {
            if (retrofit == null) {
                val logging = HttpLoggingInterceptor()
                logging.setLevel(HttpLoggingInterceptor.Level.HEADERS)
                logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                val httpClient = OkHttpClient.Builder()
                    .callTimeout(1, TimeUnit.MINUTES)
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .addInterceptor(logging)
                val okHttpClient: OkHttpClient = httpClient.build()
                retrofit = Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(VariablesUtils().baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit
        }
    }

}