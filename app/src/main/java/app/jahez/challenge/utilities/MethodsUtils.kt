package app.jahez.challenge.utilities

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

class MethodsUtils {
    /**
     * round double numbers.
     * @value: double number to round it.
     * @places: count of numbers after comma
     *
     * @return rounded number
     */
    fun round(value: Double, places: Int): Double {
        var value = value
        require(places >= 0)
        val factor = Math.pow(10.0, places.toDouble()).toLong()
        value *= factor
        val tmp = Math.round(value)
        return tmp.toDouble() / factor
    }

    /**
     * check if current time is between tow times
     * @value: given time
     *
     * @return Boolean value
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun checkStatus(value: String): Boolean {
        try {
            val hours = value.split("-")
            val t1 = hours[0]
            val t2 = hours[1].split("&")[0]

            val format: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm")

            val s = convertTime(t1)
            val e = convertTime(t2)

            val t = LocalTime.now().format(format)
            Log.i("checkStatus", t.toString())

            val startTime: LocalTime = LocalTime.parse(s, format)
            val endTime: LocalTime = LocalTime.parse(e, format)
            val targetTime: LocalTime = LocalTime.parse(t, format)

            return if (startTime.isAfter(endTime)) {
                targetTime.isBefore(endTime) || targetTime.isAfter(startTime)
            } else {
                targetTime.isBefore(endTime) && targetTime.isAfter(startTime)
            }
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.i("checkStatus", e.message!!)
        }
        return false
    }

    /**
     * convert time from 12 hours format to 24 hours format
     * @hour: String to convert
     *
     * @return converted hour
     */
    fun convertTime(hour: String): String {
        val df: DateFormat = SimpleDateFormat("hh:mm aa")
        val outputFormat: DateFormat = SimpleDateFormat("HH:mm")
        var date: Date? = null
        var output: String? = null
        date = df.parse(hour)
        output = outputFormat.format(date)
        return output
    }


    fun loadLocale(context: Context) {
        val sharedPreferences = context.getSharedPreferences("Settings", Activity.MODE_PRIVATE)
        val language = sharedPreferences.getString("lang", "en")
        VariablesUtils.language = language!!
        setLocaleLang(language, context)
    }

    fun setLocaleLang(lang: String, context: Context) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val resources = context.resources
        val configuration = resources.configuration
        configuration.setLocale(locale)
        resources.updateConfiguration(configuration, resources.displayMetrics)

        val editor = context.getSharedPreferences("Settings", Context.MODE_PRIVATE).edit()
        editor.putString("lang", lang)
        editor.apply()
    }

    fun internetCheck(c: Context): Boolean {
        val cmg = c.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            // Android 10+
            cmg.getNetworkCapabilities(cmg.activeNetwork)?.let { networkCapabilities ->
                return networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                        || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
            }
        } else {
            return cmg.activeNetworkInfo?.isConnectedOrConnecting == true
        }

        return false
    }

}

val mUtils = MethodsUtils()